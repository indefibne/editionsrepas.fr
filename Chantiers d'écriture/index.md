---
title: Chantiers d'écriture
header:
    image: Images/IMG_Chantier3.jpg
---

# Chantiers d'écriture


*Pourquoi il y a-t-il si peu de récits de projets réels dans nos librairies ?*
*Les porteurs de projets ont-ils envie de se raconter ?*
*Une lecture peut-elle amener de nouvelles personnes à se lancer dans l'alternative ?*
*Comment accompagner la naissance de ces récits qui deviendront des livres et des leviers de changement ?*


A tous ces questionnements, nous tentons de répondre par notre expérience de l'écriture, de la mise en récit et des dynamiques spécifiques aux collectifs autogérés. Avec un bagage d'éducation populaire, nous faisons le pari de **stages d'accompagnement à l'écriture collective** que nous avons nommé : les **chantiers d'écriture**.
Nous proposons ainsi à des structures et des collectifs menant un projet alternatif et solidaire de retracer leur parcours pour en tirer un récit commun. A partir de chaque expérience, unique, nous voulons en ressortir des savoirs, des anecdotes, des (anti-)exemples et des réflexions sur leur modèle, pour les partager au grand public par la **publication d'un livre petit format aux éditions REPAS.**


- [Appel à contribution](./appel-a-contributions/) {.cta}
- [Récits de chantiers](./recits-de-chantiers/) {.cta}

