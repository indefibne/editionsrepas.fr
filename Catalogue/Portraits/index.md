---
Collection: Constance Social Club
DépotLégal: 2015
Auteur: Edmond Baudoin
Titre: Portraits
SousTitre: Faux-la-Montagne
ISBN : 978-2-919272-08-2
Pages: 192 pages
Prix: 16
Etat: Disponible
Résumé: |
    Quand Edmond Baudoin dessine les habitants d'un village limousin... et quand ces habitants lui racontent leur village.

    Département de la Creuse, au centre du Limousin, Faux-la-Montagne fait partie de ces communes rurales les moins denses de France. Après les livres "Télé Millevaches" qui raconte l'histoire d'une télévision locale, "Scions... travaillait autrement" sur l'aventure d'une scierie autogérée qui fonctionne depuis 1988, c'est le dessinateur Baudouin qui réalise un portrait du village.

    L’idée est née de réaliser un portrait du village qui ne soit pas collectif, mais pointilliste. Laetitia Carton, qui vit à Faux depuis huit ans et est depuis 2014 conseillère municipale, demande à son ami Edmond Baudoin de venir dessiner les gens de Faux. Soixante-et-un d’entre eux (15 % de la population) ont accepté de se prêter à la pose. Mais Baudoin, lorsqu’il crayonne, n’aime guère le silence. Il veut connaître celui qu’il croque. Il lui pose des questions, l’interroge sur sa vie, sur le village et son avenir. Le modèle parle. Laetitia enregistre. Toute une équipe bénévole transcrira les enregistrements et on exposera l’ensemble, les dessins et les textes, dans la salle de la mairie où chacun pourra ensuite venir les voir et les lire. Ce livre est le résultat de cette expérience. Avec un parti pris : publier tels quels les témoignages des habitants. Sans réécriture mais avec l’authenticité d’une parole brute, directe, sincère.
Tags:
- Développement rural
- témoignages
- dessinateur
- alternatives
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/portraits-faux-la-montagne-edmond-baudoin?_ga=2.210317333.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272082-portraits-faux-la-montagne-baudoin/
Couverture:
Diffusion: Editions REPAS
---
