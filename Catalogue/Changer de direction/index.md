---
Edition: Editions REPAS
DépotLégal: 2022
Autrice: La Manufacture Coopérative
Titre: Changer de direction ?
SousTitre: Récits croisés de transmissions dans les coopératives
Préface: Postface de Béatrice Poncin
Collection: Hors collection
ISBN: 978-2-919272-18-1
Pages: 136 pages
Prix: 10
Etat: Disponible
Résumé: |
    Passer le relai, changer de gérant·e, transmettre la société, choisir une nouvelle équipe de direction : voilà des moments majeurs et souvent critiques dans la vie d’une entreprise. C’est aussi un sujet crucial et d’actualité pour les entreprises de l’économie sociale et solidaire, et en particulier pour les coopératives dont la gouvernance implique une démarche collective et partagée. Qu’il s’agisse du départ des personnes fondatrices ou de celui des générations suivantes, ces périodes de transmission sont fréquemment à risque et laissent rarement indemnes les individus et les équipes.

    Ce livre propose les témoignages de douze personnes qui ont toutes porté un mandat de direction dans une coopérative d’activités et d’emploi (CAE) et vécu une expérience de transmission. Comment l’ont-elles traversée ? Quels obstacles ont-elles rencontrés ? Quelles questions se sont-elles posées et comment y ont-elles répondu ? Ce livre est un puzzle fait de leurs récits croisés.

    Laissez-vous porter par ces expériences singulières qui ouvrent sur une compréhension sensible de ces aventures collectives. Puissent ces récits nous éclairer sur les chemins individuels et collectifs qui gravitent, se rencontrent et se confrontent parfois lorsque les coopératives passent d’une période à une autre, d’une direction à une autre.
Tags:
- Coopérative d'activité et d'emploi
- Collectif
- Gouvernance
- Transmission
- Economie sociale et solidaire
SiteWeb: http://manufacture.coop/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/changer-de-direction-la-manufacture-cooperative
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272181-changer-de-direction-recits-croises-de-transmissions-dans-les-cooperatives-la-manufacture-cooperative/
Bandeau: Nouveauté
AccueilOrdre: 2
Couverture:
Couleurs:
  Fond: '#539BAD'
  Titre: '#fff'
  Texte: '#fbfcf8'
  PageTitres: '#5194a0'
Fonts:
  Titre: highvoltage rough
  SousTitre: inherit
---

## Les autrices

Élément de la recherche-action « Transmissions coopératives », ce livre a été imaginé et mis en œuvre par la Manufacture coopérative, en partenariat avec le Ladyss et la MYNE.

La **[Manufacture coopérative](http://manufacture.coop/)** est une coopérative de recherche-action née en 2015 d’une rencontre
entre personnes issues du monde des coopératives et de celui de la recherche. Elles font fait le
même constat : la nécessité de penser et d’agir pour produire concrètement des alternatives et
soutenir des processus de transformation sociale avec et par la coopération.

## Extrait

> Je suis arrivée 2 ou 3 ans après la création donc depuis un bon moment, mais Alain [l’ancien gérant], il est vraiment ancré. Une des craintes que j’ai exprimée, c’est comment trouver notre place, sachant qu’il est encore là et qu’il a toujours eu une place importante. Quand il y avait un problème, si je donnais une réponse qui ne convenait pas : hop, als passaient par lui.
> On fait déjà pas mal de choses sans qu’Alain soit concerné : des clôtures de comptes d’entreprenaires un peu plus difficiles, des recrutements sur la fin d’année. Avec Miguel [mon co-DG], on a pris déjà ces responsabilités là. Et Alain laisse aussi la place. Dès l’instant où il a fait son accompagnement, qu’il a acté qu’il était prêt à laisser sa place, il s’est petit à petit effacé sur certaines choses ; et nous, à l’inverse, on a pris notre place, réellement notre place pour pouvoir gérer les choses.
> -- page 44


## Le mot des éditeurs

* Des témoignages aussi rares que délicats sur un sujet souvent tabou : prendre ou quitter des responsabilités professionnelles est en effet une aventure humaine à part entière.
* Ce livre veut rendre accessible les problématiques internes rencontrées dans les entreprises coopératives et offrir une prise de recul sur des dynamiques humaines complexes.
* Inspiré du livre *Cinq mains coupées* de Sophie Divry (Seuil, 2020), *Changer de direction ?* propose une mise en récit de témoignages dénuée d’analyse qui laisse une large place au ressenti et à l’interprétation du lecteur. Un courant littéraire qui donne à voir ce qu’il y a de commun et d’universel au travers de trajectoires particulières.


